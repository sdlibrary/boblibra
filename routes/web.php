<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Auth::routes();
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/home', 'HomeController@index')->name('home');

Route::prefix('search')->group(function() {
    Route::get('/', 'SearchController@index')->name('search');
    Route::get('find', 'SearchController@find');
    // Route::get('', 'SearchController@search')->name('search.search');
});

Route::prefix('books')->group(function() {
    Route::get('/', 'BooksController@index')->name('books');
    Route::get('{id}', 'BooksController@detail')->name('books.detail');
});


Route::prefix('admin')->namespace('Admin')->middleware('auth')->group(function() {
    Route::get('/', 'DashboardController@index')->name('admin');
    Route::prefix('authors')->group(function() {
        Route::get('/', 'AuthorsController@index')->name('admin.authors');
        Route::get('{id}', 'AuthorsController@detail')->name('admin.author.detail');
        Route::post('create', 'AuthorsController@create')->name('admin.authors.create');
    });
    Route::prefix('books')->group(function() {
        Route::get('/', 'BooksController@index')->name('admin.books');
        Route::get('archive', 'BooksController@archive')->name('admin.books.archive');
        Route::get('{id}', 'BooksController@detail')->name('admin.books.detail');
        Route::delete('{id}', 'BooksController@delete')->name('admin.books.delete');
        Route::post('archive/{id}', 'BooksController@restore')->name('admin.books.restore');
        Route::delete('archive/{id}', 'BooksController@hardRemove')->name('admin.books.hard-remove');
    });
    Route::prefix('publishers')->group(function() {
        Route::get('/', 'PublishersController@index')->name('admin.publishers');
        Route::get('{id}', 'PublishersController@detail')->name('admin.publishers.detail');
    });
    Route::prefix('tags')->group(function() {
        Route::get('/', 'TagsController@index')->name('admin.tags');
        Route::post('add', 'TagsController@add')->name('admin.tags.add');
    });
    Route::prefix('dbdump')->group(function() {
        Route::get('/', 'DbDumpController@index')->name('admin.dbdump');
        Route::post('/', 'DbDumpController@dump')->name('admin.dbdump');
        Route::get('download', 'DbDumpController@download')->name('admin.dbdump.download');
    });
});

Route::prefix('php-learning')->namespace('Admin')->group(function() {
    Route::get('/', 'ParserController@index')->name('admin.parser');
    Route::post('a', 'ParserController@parseAuthors')->name('admin.parse.authors');
    Route::get('a/test', 'ParserController@receiveAuthor');
    Route::get('b/test', 'ParserController@receiveBook');
    Route::get('p/test', 'ParserController@receivePublisher');
});
