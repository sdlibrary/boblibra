<?php

use Illuminate\Database\Seeder;

class InitialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->addMockAuthorForBooksWithoutAuthor();
    }

    private function addMockAuthorForBooksWithoutAuthor()
    {
        DB::table('authors')->insert([
            'name' => 'Без автора',
            'is_hidden' => true
        ]);
        DB::table('publishers')->insert([
            'name' => 'Без издателя',
            'is_hidden' => true
        ]);
    }
}
