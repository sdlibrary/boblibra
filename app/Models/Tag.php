<?php

namespace App\Models;

class Tag extends \App\Models\Base\Tag
{
	protected $fillable = [
		'name',
		'code'
	];
}
