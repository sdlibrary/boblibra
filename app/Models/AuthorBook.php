<?php

namespace App\Models;

class AuthorBook extends \App\Models\Base\AuthorBook
{
	protected $fillable = [
		'author_id',
		'book_id',
		'unbound_author_id'
	];
}
