<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 04 Jun 2018 02:19:59 +0300.
 */

namespace App\Models\Base;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Publisher
 * 
 * @property int $id
 * @property string $name
 * @property int $ozon_id
 * @property bool $is_hidden
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $books
 *
 * @package App\Models\Base
 */
class Publisher extends Eloquent
{
	protected $casts = [
		'ozon_id' => 'int',
		'is_hidden' => 'bool'
	];

	public function books()
	{
		return $this->belongsToMany(\App\Models\Book::class)
					->withPivot('id', 'unbound_publisher_id');
	}
}
