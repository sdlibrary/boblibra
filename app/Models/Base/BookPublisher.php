<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 03 Jun 2018 16:36:48 +0300.
 */

namespace App\Models\Base;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class BookPublisher
 * 
 * @property int $id
 * @property int $publisher_id
 * @property int $book_id
 * @property int $unbound_publisher_id
 * 
 * @property \App\Models\Book $book
 * @property \App\Models\Publisher $publisher
 *
 * @package App\Models\Base
 */
class BookPublisher extends Eloquent
{
	protected $table = 'book_publisher';
	public $timestamps = false;

	protected $casts = [
		'publisher_id' => 'int',
		'book_id' => 'int',
		'unbound_publisher_id' => 'int'
	];

	public function book()
	{
		return $this->belongsTo(\App\Models\Book::class);
	}

	public function publisher()
	{
		return $this->belongsTo(\App\Models\Publisher::class);
	}
}
