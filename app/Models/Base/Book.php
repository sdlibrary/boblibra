<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 11 Jun 2018 14:00:40 +0300.
 */

namespace App\Models\Base;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Book
 * 
 * @property int $id
 * @property string $name
 * @property string $original_name
 * @property string $format
 * @property int $pages
 * @property int $year
 * @property string $cover
 * @property string $description
 * @property int $ozon_id
 * @property string $deleted_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $authors
 * @property \Illuminate\Database\Eloquent\Collection $publishers
 *
 * @package App\Models\Base
 */
class Book extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'pages' => 'int',
		'year' => 'int',
		'ozon_id' => 'int'
	];

	public function authors()
	{
		return $this->belongsToMany(\App\Models\Author::class)
					->withPivot('id', 'unbound_author_id');
	}

	public function publishers()
	{
		return $this->belongsToMany(\App\Models\Publisher::class)
					->withPivot('id', 'unbound_publisher_id');
	}
}
