<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 20 May 2018 19:32:31 +0300.
 */

namespace App\Models\Base;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class AuthorBook
 * 
 * @property int $id
 * @property int $author_id
 * @property int $book_id
 * @property int $unbound_author_id
 * 
 * @property \App\Models\Author $author
 * @property \App\Models\Book $book
 *
 * @package App\Models\Base
 */
class AuthorBook extends Eloquent
{
	protected $table = 'author_book';
	public $timestamps = false;

	protected $casts = [
		'author_id' => 'int',
		'book_id' => 'int',
		'unbound_author_id' => 'int'
	];

	public function author()
	{
		return $this->belongsTo(\App\Models\Author::class);
	}

	public function book()
	{
		return $this->belongsTo(\App\Models\Book::class);
	}
}
