<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 20 May 2018 19:32:31 +0300.
 */

namespace App\Models\Base;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Author
 * 
 * @property int $id
 * @property string $name
 * @property int $ozon_id
 * @property bool $is_hidden
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $books
 *
 * @package App\Models\Base
 */
class Author extends Eloquent
{
	protected $casts = [
		'ozon_id' => 'int',
		'is_hidden' => 'bool'
	];

	public function books()
	{
		return $this->belongsToMany(\App\Models\Book::class)
					->withPivot('id', 'unbound_author_id');
	}
}
