<?php

namespace App\Models;

class Publisher extends \App\Models\Base\Publisher
{
	protected $fillable = [
		'name',
		'ozon_id',
		'is_hidden'
	];

	public function getOzonUrlAttribute()
	{
		if (empty($this->ozon_id)) {
			return false;
		}
		return \App\Utilities\OzonSettings::getUrlWithId('publishers', $this->ozon_id);
	}
}
