<?php

namespace App\Models;
use Nicolaslopezj\Searchable\SearchableTrait;

class Book extends \App\Models\Base\Book
{
	use SearchableTrait;

    protected $searchable = [
        'columns' => [
            'books.name' => 10,
            // 'authors.name' => 5,
            // 'publishers.name' => 3,
        ],
        // 'joins' => [
        //     'authors' => ['users.id','authors.user_id'],
        // ],
    ];

	protected $fillable = [
		'name',
		'author_id',
		'original_name',
		'format',
		'pages',
		'year',
		'publisher_id',
		'cover',
		'ozon_id',
	];

	public function getOzonUrlAttribute()
	{
		if (empty($this->ozon_id)) {
			return false;
		}
		return \App\Utilities\OzonSettings::getUrlWithId('books', $this->ozon_id);
	}

	public function authorsNames($withLink = false)
	{
		$names = [];
		$authors = $this->authors()->get();
		foreach ($authors as $author) {
			$names[] = $withLink
				? '<a href="/admin/authors/' . $author->id . '/">' . $author->name . '</a> '
				: $author->name;
		}
		return implode(', ', $names);
	}

	public function publishersNames($withLink = false)
	{
		$names = [];
		$publishers = $this->publishers()->get();
		foreach ($publishers as $publisher) {
			$names[] = $withLink
				? '<a href="/admin/publishers/' . $publisher->id . '/">' . $publisher->name . '</a> '
				: $publisher->name;
		}
		return implode(', ', $names);
	}
}
