<?php

namespace App\Models;

class Author extends \App\Models\Base\Author
{
	protected $fillable = [
		'name',
		'ozon_id',
		'is_hidden'
	];

	public function getOzonUrlAttribute()
	{
		if (empty($this->ozon_id)) {
			return false;
		}
		return \App\Utilities\OzonSettings::getUrlWithId('authors', $this->ozon_id);
	}
}
