<?php

namespace App\Models;

class BookPublisher extends \App\Models\Base\BookPublisher
{
	protected $fillable = [
		'publisher_id',
		'book_id',
		'unbound_publisher_id'
	];
}
