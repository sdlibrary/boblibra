<?php

namespace App\Utilities;

class OzonSettings
{
    private static $idPlaceholder = "#ID#";
    private static $url = [
        'authors' => "https://www.ozon.ru/person/#ID#/",
        'books' => "https://www.ozon.ru/context/detail/id/#ID#/",
        'publishers' => "https://www.ozon.ru/brand/#ID#/",
    ];

    public static function getUrlMask($key)
    {
        return self::$url[$key] ?? false;
    }

    public static function getUrlWithId($key, $id)
    {
        if (empty(self::$url[$key])) {
            return false;
        }
        return str_replace(self::$idPlaceholder, $id, self::$url[$key]);
    }
}