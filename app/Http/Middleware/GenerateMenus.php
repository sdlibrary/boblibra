<?php

namespace App\Http\Middleware;

use Closure;

class GenerateMenus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        \Menu::make('MenuAdminLeft', function ($menu) {
            // $menu->add('Рабочий стол', 'admin')
            //     ->append('</p>')
            //     ->prepend('<i class="ti-pie-chart"></i> <p>');
            $menu->add('Книги', ['route' => 'admin.books'])
                ->append('</p>')
                ->prepend('<i class="ti-book"></i> <p>');
            $menu->add('Авторы', ['route' => 'admin.authors'])
                ->append('</p>')
                ->prepend('<i class="ti-ink-pen"></i> <p>');
            $menu->add('Издатели', ['route' => 'admin.publishers'])
                ->append('</p>')
                ->prepend('<i class="ti-world"></i> <p>');
            $menu->add('Дамп БД', ['route' => 'admin.dbdump'])
                ->append('</p>')
                ->prepend('<i class="ti-download"></i> <p>');
            // $menu->add('Теги', ['route' => 'admin.tags'])
            //     ->append('</p>')
            //     ->prepend('<i class="ti-tag"></i> <p>');
            // $menu->add('Парсер', ['route' => 'admin.parser'])
            //     ->append('</p>')
            //     ->prepend('<i class="ti-wand"></i> <p>');
        });
        return $next($request);
    }
}
