<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DbDumpController extends Controller
{
    public function index(Request $request)
    {
        $directory = $this->getDumpDirectory(true);
        $files = \File::allFiles($directory);
        $list = [];
        foreach ($files as $file) {
            $list[] = [
                'path' => $file->getRelativePathName(),
                'size' => $this->formatBytes($file->getSize())
            ];
        }
        return view('admin.dbdump', ['list' => $list]);
    }

    public function dump()
    {
        \Artisan::call('db:dump');
        $directory = $this->getDumpDirectory();
        return redirect()->back()->with('message.success', "Дамп сгенерирован в директории " . $directory);
    }

    public function download(Request $request)
    {
        $path = $request->get('path');
        $filepath = $this->getDumpDirectory(true) . $path;
        if (!empty($path) && is_readable($filepath)) {
            return response()->download($filepath);
        }
        return redirect()->back()->with('message.error', "Нет файла <b>" . $filepath . "</b>");
    }

    private function getDumpDirectory($general = false)
    {
        $ds = DIRECTORY_SEPARATOR;
        return database_path() . $ds . 'backups' . $ds . ($general ? '' : date('Y-m') . $ds);
    }

    private function formatBytes($bytes, $precision = 2) {
        $units = array('B', 'KB', 'MB', 'GB', 'TB');

        $bytes = max($bytes, 0);
        $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
        $pow = min($pow, count($units) - 1);

        // Uncomment one of the following alternatives
        $bytes /= pow(1024, $pow);
        // $bytes /= (1 << (10 * $pow));

        return round($bytes, $precision) . ' ' . $units[$pow];
    }
}
