<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Utilities\Constants;
use App\Utilities\OzonSettings;
use App\Models\Author;
use App\Models\Book;
use App\Models\AuthorBook;
use App\Models\BookPublisher;
use App\Models\Publisher;
use App\Models\Tag;

class ParserController extends Controller
{
    private $idPlaceholder = '#ID#';

    private $originMasks = [
        'authors' => "/ozon\.ru\/person\/(\d+?)\//i",
        'books' => "/ozon\.ru\/context\/detail\/id\/(\d+?)\//i",
        'publishers' => "/ozon\.ru\/brand\/(\d+?)\//i",
    ];

    private $data = [];

    public function index()
    {
        return view('admin.parser');
    }

    public function receiveAuthor(Request $request)
    {
        try {
            if ($this->getDataFromRequest($request)) {
                $name = $this->data['name'] ?? false;
                $originId = $this->getOriginId('authors', $this->data['origin'] ?? false);
                if ($name && $originId) {
                    $author = Author::create([
                        'name' => $name,
                        'ozon_id' => $originId
                    ]);
                    if (!empty($author) && $author->id) {
                        $this->bindAuthorId($author);
                        echo json_encode(['success' => true]);
                        return;
                    }
                }
            }
        } catch (\Exception $e) {}
        echo json_encode(['success' => false]);
        return;
    }

    public function receivePublisher(Request $request)
    {
        try {
            if ($this->getDataFromRequest($request)) {
                $name = $this->data['name'] ?? false;
                $originId = $this->getOriginId('publishers', $this->data['origin'] ?? false);
                if ($name && $originId) {
                    $publisher = Publisher::create([
                        'name' => $name,
                        'ozon_id' => $originId
                    ]);
                    if (!empty($publisher) && $publisher->id) {
                        $this->bindPublisherId($publisher);
                        echo json_encode(['success' => true]);
                        return;
                    }
                }
            }
        } catch (\Exception $e) {}
        echo json_encode(['success' => false]);
        return;
    }

    public function receiveBook(Request $request)
    {
        try {
            if ($this->getDataFromRequest($request)) {
                // echo "<pre>this->data - "; print_r($this->data); echo "</pre>";
                $bookAuthorsData = $this->getBookAuthorsData($this->data['authors'] ?? false);
                $bookPublishersData = $this->getBookPublishersData($this->data['publishers'] ?? false);
                // echo "<pre>bookAuthorsData - "; print_r($bookAuthorsData); echo "</pre>";
                $name = $this->data['name'] ?? false;
                $year = $this->data['year'] ?? false;
                $originalName = $this->data['original_name'] ?? false;
                $format = $this->data['format'] ?? false;
                $pages = $this->data['pages'] ?? false;
                $publisher_id = $this->data['publisher_id'] ?? false;
                $cover = $this->data['cover'] ?? false;
                $originId = $this->getOriginId('books', $this->data['origin'] ?? false);
                if ($name && $originId) {
                    $bookData = [
                        'name' => $name,
                        'ozon_id' => $originId
                    ];
                    $this->appendData($bookData, [
                        'year' => $year,
                        'original_name' => $originalName,
                        'format' => $format,
                        'pages' => $pages,
                        'cover' => $cover,
                    ]);
                    $book = Book::create($bookData);
                    if (!empty($book) && $book->id) {
                        if (
                            $this->saveBookAuthorsData($book->id, $bookAuthorsData)
                            && $this->saveBookPublishersData($book->id, $bookPublishersData)
                        ) {
                            echo json_encode(['success' => true, 'book' => $book]);
                            return;
                        }
                    }
                }
            }
        } catch (\Exception $e) {
            echo json_encode(['success' => false, 'exception' => '[' . $e->getCode() . '] ' . $e->getMessage(), 'trace' => $e->getTraceAsString()]);
            return;
        }
        echo json_encode(['success' => false]);
        return;
    }

    private function appendData(&$data, $newDataArray)
    {
        foreach ($newDataArray as $name => $item) {
            if (!empty($item)) {
                $data[$name] = $item;
            }
        }
    }

    private function getDataFromRequest(Request $request)
    {
        $dataRaw = $request->get('test');
        $this->data = empty($dataRaw) ? [] : json_decode(urldecode(base64_decode($dataRaw)), true);
        return !empty($this->data);
    }

    private function getBookAuthorsData($authorIdsList)
    {
        $bookAuthorsData = [];
        if (empty($authorIdsList) || !is_array($authorIdsList)) {
            return [];
        }
        foreach ($authorIdsList as $authorId) {
            $authorId = (int)$authorId;
            if (!$authorId) {
                // Logger::couldNotCreateAuthor($author);
                continue;
            }
            $author = Author::where('ozon_id', $authorId)->get()->first();
            if (empty($author) || !$author->id) {
                $bookAuthorsData[$authorId] = [
                    'author_id' => Constants::DEFAULT_AUTHOR_ID,
                    'unbound_author_id' => $authorId,
                ];
            } else {
                $bookAuthorsData[$authorId] = [
                    'author_id' => $author->id,
                ];
            }
        }
        return $bookAuthorsData;
    }

    private function saveBookAuthorsData($bookId, array $bookAuthorsData)
    {
        $result = true;
        foreach ($bookAuthorsData as $data) {
            $data['book_id'] = $bookId;
            $authorBook = AuthorBook::create($data);
            // echo "<pre>authorBook - "; print_r($authorBook); echo "</pre>";
            if (empty($authorBook)) {
                // Logger::couldNotCreateBookAuthor($data);
                $result = false;
            }
        }
        return $result;
    }

    private function getBookPublishersData($publisherIdsList)
    {
        $bookPublishersData = [];
        if (empty($publisherIdsList) || !is_array($publisherIdsList)) {
            return [];
        }
        foreach ($publisherIdsList as $publisherId) {
            $publisherId = (int)$publisherId;
            if (!$publisherId) {
                // Logger::couldNotCreatePublisher($publisher);
                continue;
            }
            $publisher = Publisher::where('ozon_id', $publisherId)->get()->first();
            if (empty($publisher) || !$publisher->id) {
                $bookPublishersData[$publisherId] = [
                    'publisher_id' => Constants::DEFAULT_PUBLISHER_ID,
                    'unbound_publisher_id' => $publisherId,
                ];
            } else {
                $bookPublishersData[$publisherId] = [
                    'publisher_id' => $publisher->id,
                ];
            }
        }
        return $bookPublishersData;
    }

    private function saveBookPublishersData($bookId, array $bookPublishersData)
    {
        $result = true;
        foreach ($bookPublishersData as $data) {
            $data['book_id'] = $bookId;
            $bookPublisher = BookPublisher::create($data);
            // echo "<pre>bookPublisher - "; print_r($bookPublisher); echo "</pre>";
            if (empty($bookPublisher)) {
                // Logger::couldNotCreateBookPublisher($data);
                $result = false;
            }
        }
        return $result;
    }

    private function getOriginId(string $mask, $originUrl)
    {
        if (!$originUrl) {
            return false;
        }
        preg_match($this->originMasks[$mask], $originUrl, $originIdMatches);
        return empty($originIdMatches[1]) ? false : $originIdMatches[1];
    }

    private function bindAuthorId(Author $author)
    {
        $authorBooksList = AuthorBook::where('unbound_author_id', $author->ozon_id)->get();
        if (!empty($authorBooksList)) {
            foreach ($authorBooksList as $ab) {
                $ab->author_id = $author->id;
                $ab->unbound_author_id = null;
                $ab->save();
            }
        }
    }

    private function bindPublisherId(Publisher $publisher)
    {
        $publisherBooksList = BookPublisher::where('unbound_publisher_id', $publisher->ozon_id)->get();
        if (!empty($publisherBooksList)) {
            foreach ($publisherBooksList as $ab) {
                $ab->publisher_id = $publisher->id;
                $ab->unbound_publisher_id = null;
                $ab->save();
            }
        }
    }
}
