<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Publisher;

class PublishersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the admin Publishers section.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $publishers = Publisher::where(['is_hidden' => false])->get();
        return view('admin.publishers', ['list' => $publishers]);
    }

    /**
     * Show the admin Publisher detail page.
     *
     * @return \Illuminate\Http\Response
     */
    public function detail($id)
    {
        $data = [
            'publisher' => false,
        ];
        $publisher = Publisher::with('books')->where('is_hidden', false)->find($id);
        if (empty($publisher) || !$publisher->id) {
            return response()->view('admin.detail.publisher', $data, 404);
        }
        $data['publisher'] = $publisher;
        return view('admin.detail.publisher', $data);
    }
}
