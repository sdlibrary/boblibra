<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Utilities\Constants;
use App\Models\Book;

class BooksController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the admin Books section.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $books = Book::with('authors')->limit(20)->get();
        return view('admin.books', ['list' => $books]);
    }

    /**
     * Show the admin Book detail page.
     *
     * @return \Illuminate\Http\Response
     */
    public function detail($id)
    {
        $data = [
            'book' => false,
        ];
        $book = Book::with('authors', 'publishers')->find($id);
        if (empty($book) || !$book->id) {
            return response()->view('admin.detail.book', $data, 404);
        }
        $data['book'] = $book;
        return view('admin.detail.book', $data);
    }

    public function archive()
    {
        $books = Book::onlyTrashed()->with('authors')->get();
        return view('admin.books', ['list' => $books, 'isArchive' => true]);
    }

    public function delete($id)
    {
        $book = Book::find($id);
        $name = $book->name;
        $book->delete();

        return redirect()->back()->with('message.success', "Удалена книга: <b>" . $name . "</b>");
    }
    public function restore($id)
    {
        $book = Book::onlyTrashed()->find($id);
        $name = $book->name;
        $book->restore();

        return redirect()->back()->with('message.success', "Восстановлена книга: <b>" . $name . "</b>");
    }
    public function hardRemove($id)
    {
        $book = Book::onlyTrashed()->find($id);
        $name = $book->name;
        $book->forceDelete();

        return redirect()->back()->with('message.success', "Окончательно удалена книга: <b>" . $name . "</b>");
    }

    private function create($data)
    {

        if (empty($data['authors'])) {
            $data['authors'][] = Constants::DEFAULT_AUTHOR_ID;
        }
        return Book::create($data);
    }
}
