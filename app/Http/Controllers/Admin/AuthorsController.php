<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Author;

class AuthorsController extends Controller
{
    /**
     * Show the admin Authors section.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $authors = Author::where(['is_hidden' => false])->get();
        return view('admin.authors', ['list' => $authors]);
    }

    /**
     * Show the admin Author detail page.
     *
     * @return \Illuminate\Http\Response
     */
    public function detail($id)
    {
        $data = [
            'author' => false,
        ];
        $author = Author::with('books')->where('is_hidden', false)->find($id);
        if (empty($author) || !$author->id) {
            return response()->view('admin.detail.author', $data, 404);
        }
        $data['author'] = $author;
        return view('admin.detail.author', $data);
    }

    /**
     * Создать нового автора.
     *
     * @param  Request  $request
     * @return Response
     */
    public function create(Request $request)
    {
        // validate $data
        $validatedData = $request->validate([
            'name' => 'required|max:255',
        ]);

        $author = Author::create([
            'name' => $request->name,
        ]);

        if (empty($author)) {
            return redirect()->back()->with('message.error', "Произошла ошибка при добавлении автора");
        }
        return redirect()->back()->with('message.success', "Автор [" . $author->id . "] " . $author->name . " добавлен");
    }
}
