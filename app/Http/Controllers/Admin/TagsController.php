<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Tag;

class TagsController extends Controller
{
    /**
     * Show the admin Tags section.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tags = Tag::all();
        return view('admin.tags', ['list' => $tags]);
    }

    /**
     * Добавить нового тег.
     *
     * @param  Request  $request
     * @return Response
     */
    public function add(Request $request)
    {
        // validate $data
        $validatedData = $request->validate([
            'name' => 'required|max:50',
            'code' => 'required|max:50',
        ]);

        $tag = Tag::create([
            'name' => $request->name,
            'code' => $request->code,
        ]);

        if (empty($tag)) {
            return redirect()->back()->with('message.error', "Произошла ошибка при добавлении тега");
        }
        return redirect()->back()->with('message.success', "Тег [" . $tag->id . "] " . $tag->name . " / " . $tag->code . " добавлен");
    }
}
