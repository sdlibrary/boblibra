<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Book;

class SearchController extends Controller
{

    /**
     * Отобразить страницу поиска.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = $request->query('query');

        // if (isset($query)) {
        //     $validatedData = $request->validate([
        //         'query' => 'required|max:255',
        //     ]);
        //     return $this->search($query);
        // }
        return view('search.index');
    }

    // /**
    //  * Искать.
    //  *
    //  * @return \Illuminate\Http\Response
    //  */
    // public function search(string $searchQuery)
    // {
    //     $results = $this->find($searchQuery);
    //     return view('search.index', ['results' => $results]);
    // }

    // public function find(string $query)
    // {
    //     $books = \App\Models\Book::where('name', 'LIKE', '%' . str_replace('%', '\\%', $query) . '%')->get();
    //     $authors = \App\Models\Author::where('name', 'LIKE', '%' . str_replace('%', '\\%', $query) . '%')->get();
    //     return [
    //         'authors' => $authors,
    //         'books' => $books,
    //     ];
    // }

    public function find(Request $request)
    {
        return Book::search($request->get('q'))->get();
    }
}
