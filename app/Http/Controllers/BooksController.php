<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Author;
use App\Models\Book;

class BooksController extends Controller
{
    public function index(Request $request)
    {
        $limit = 20;
        $searchQuery = $request->get('query');
        $doSearchAuthors = $request->get('search-authors');

        if ($searchQuery) {
            if (!empty($doSearchAuthors)) {
                $books = Book::with('authors')->whereHas('authors', function($query) use ($searchQuery) {
                    $query->where('name', 'LIKE', '%' . str_replace('%', '\\%', $searchQuery) . '%');
                });
            } else {
                $books = Book::with('authors')->where('name', 'LIKE', '%' . str_replace('%', '\\%', $searchQuery) . '%');
            }
        } else {
            $books = Book::with('authors');
        }
        return view('books', [
            'list' => $books->limit($limit)->get(),
            'doSearchAuthors' => $doSearchAuthors
        ]);
    }

    public function detail($id)
    {
        $data = [
            'book' => false,
        ];
        $book = Book::with('authors', 'publishers')->find($id);
        if (empty($book) || !$book->id) {
            return response()->view('admin.detail.book', $data, 404);
        }
        $data['book'] = $book;
        return view('detail.book', $data);
    }
}
