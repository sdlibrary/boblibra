@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="header">
                    <h4 class="title">Создать дамп базы данных</h4>
                </div>
                <div class="content">
                    {{Form::open(['method'  => 'POST', 'route' => ['admin.dbdump']])}}
                        {{Form::button('<i class="ti-download"></i> <b>Создать</b>', array('type' => 'submit', 'class' => 'btn btn-primary'))}}
                    {{ Form::close() }}
                    <br/>
                </div>
            </div>
        </div>
    </div>
    @include('list.admin.dbdump')
@endsection