@extends('layouts.admin')

@section('content')
    @if (empty($book))
        <div class="jumbotron">
            <h2 class="display-4">Книга не найдена</h2>
            <p>Ошибка 404</p>
        </div>
    @else
        <div class="jumbotron">
            <h2 class="display-4">{{ $book->name }}</h2>

            <hr class="my-4">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="media">
                        <div class="media-left">
                            <img class="zoom" style="max-width: 10em;" src="{{ $book->cover }}" alt="{{ $book->name }}">
                        </div>
                        <div class="media-body">
                            <dl class="dl-horizontal">
                                <dt>Название:</dt>
                                <dd>{{ $book->name }}</dd>
                                <dt>Оригинальное название:</dt>
                                <dd>{{ $book->original_name }}</dd>
                                <dt>Авторы:</dt>
                                <dd>{!! $book->authorsNames(true) !!}</dd>
                                <dt>Издатели:</dt>
                                <dd>{!! $book->publishersNames(true) !!}</dd>
                                <dt>Формат:</dt>
                                <dd>{{ $book->format }}</dd>
                                <dt>Кол-во страниц:</dt>
                                <dd>{{ $book->pages }}</dd>
                                <dt>Год:</dt>
                                <dd> {{ $book->year }}</dd>
                                <dt>Озон:</dt>
                                <dd><a  class="btn btn-primary" href="{{ $book->ozon_url }}">Открыть</a></dd>
                            </dl>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    @endif
@endsection