@extends('layouts.admin')

@section('content')
    @if (empty($publisher))
        <div class="jumbotron">
            <h2 class="display-4">Издатель не найден</h2>
            <p>Ошибка 404</p>
        </div>
    @else
        <div class="jumbotron">
            <h2 class="display-4">{{ $publisher->name }}</h2>
            <p>Издатель</p>
            <hr class="my-4">
            <p class="lead">Название: {{ $publisher->name }}</p>
            <p class="lead">Озон: <a href="{{ $publisher->ozon_url }}">{{ $publisher->ozon_url }}</a></p>
        </div>
        @if ($publisher->books()->get())
        <p>Книги издателя</p>
        <div class="row">
            @foreach ($publisher->books()->get() as $index => $book)
            <div class="col-sm-4 col-md-2">
                <div class="thumbnail">
                    <img style="width: 100%" src="{{ $book->cover }}" alt="{{ $book->name }}">
                    <div class="caption">
                        <h3>{{ $book->name }}</h3>
                        <p><small>{{ $book->authorsNames() }}</small></p>
                        <p>{{ $book->description }}</p>
                        <p><a href="/admin/books/{{ $book->id }}/" class="btn btn-primary" role="button">Перейти</a></p>
                    </div>
                </div>
            </div>
            @if ($index % 4 == 3)
        </div>
        <div class="row">
            @endif
            @endforeach
        </div>
        @else
        <p>Книг у автора не найдено</p>
        @endif
    @endif
@endsection