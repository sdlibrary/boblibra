
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">
                                    <button class="btn btn-light" type="button" data-toggle="collapse" data-target="#addAuthor"
                                        aria-expanded="{{$errors->any() ? 'true' : 'false'}}" aria-controls="addAuthor">Добавление автора</button>
                                </h4>
                            </div>
                            <div class="content">
                                <div class="collapse{{$errors->any() ? ' in' : ''}}" id="addAuthor">
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                    {!! Form::open(['route' => 'admin.authors.create', 'method' => 'post']) !!}
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    {!! Form::label('name', 'ФИО') !!}
                                                    {!! Form::text('name', '', ['class' => 'form-control border-input', 'placeholder' => "Введите имя автора"]) !!}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="text-center">
                                            <button type="submit" class="btn btn-info btn-fill btn-wd">Создать</button>
                                        </div>
                                        <div class="clearfix"></div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>