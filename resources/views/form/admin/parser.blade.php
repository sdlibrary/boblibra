
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="content">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                                {!! Form::open(['route' => 'admin.parse.authors', 'method' => 'post']) !!}
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                {!! Form::label('ids', 'ID авторов') !!}
                                                {!! Form::text('ids', '', ['class' => 'form-control border-input', 'placeholder' => "Введите ID авторов"]) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text-center">
                                        <button type="submit" class="btn btn-info btn-fill btn-wd">Парсить</button>
                                    </div>
                                    <div class="clearfix"></div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>