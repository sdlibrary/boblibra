{!! $isArchive = !empty($isArchive) !!}
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                            @if ($isArchive)
                                <h4 class="title pull-left">Удалённые книги</h4>
                                <a href="{{ route('admin.books') }}" class="btn btn-secondary pull-right">
                                    <i class="ti-book"></i>
                                </a>
                            @else
                                <h4 class="title pull-left">Список книг</h4>
                                <a href="{{ route('admin.books.archive') }}" class="btn btn-secondary pull-right">
                                    <i class="ti-archive"></i>
                                </a>
                            @endif
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table class="table table-striped">
                                    <thead>
                                        <th>#</th>
                                    	<th>Обложка</th>
                                    	<th>Заголовок</th>
                                    	<th>Автор</th>
                                    	<th>ID (ozon.ru)</th>
                                        <th>Ссылка (ozon.ru)</th>
                                        <th></th>
                                    </thead>
                                    <tbody>
                                        @foreach ($list as $el)
                                        <tr>
                                        	<td>{{ $el['id'] }}</td>
                                        	<td>
                                            @if (!empty($el['cover']))
                                                <img class="book-cover zoom" src="{{ $el['cover'] }}"/>
                                            @endif
                                            </td>
                                        	<td><a href="/admin/books/{{ $el['id'] }}">{{ $el['name'] }}</a></td>
                                        	<td>
                                            @if (!empty($el->authors()))
                                                @foreach ($el->authors()->get() as $authorIndex => $author)
                                                <a href="/admin/authors/{{ $author->id }}/">{{ $author->name }}</a>{{
                                                    $authorIndex < $el->authors()->count() - 1 ? ', ' : ''
                                                }}
                                                @endforeach
                                            @endif
                                            </td>
                                        	<td>{{ $el['ozon_id'] }}</td>
                                            <td>@if ($el['ozon_url'])<a target="_blank" href="{{ $el['ozon_url'] }}">{{ $el['ozon_url'] }}</a>@endif</td>
                                            <td>
                                            @if ($isArchive)
                                                {{Form::open(['method'  => 'POST', 'route' => ['admin.books.restore', $el->id]])}}
                                                    {{Form::button('<i class="ti-reload"></i>', array('type' => 'submit', 'class' => 'btn btn-success'))}}
                                                {{ Form::close() }}
                                                {{Form::open(['method'  => 'DELETE', 'route' => ['admin.books.hard-remove', $el->id]])}}
                                                    {{Form::button('<i class="ti-na"></i>', array('type' => 'submit', 'class' => 'btn btn-danger'))}}
                                                {{ Form::close() }}
                                            @else
                                                {{Form::open(['method'  => 'DELETE', 'route' => ['admin.books.delete', $el->id]])}}
                                                    {{Form::button('<i class="ti-close"></i>', array('type' => 'submit', 'class' => 'btn btn-danger'))}}
                                                {{ Form::close() }}
                                            @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>