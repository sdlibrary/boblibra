
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Список файлов дампа</h4>
                                <p class="category">Всего: {{ count($list) }}</p>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table class="table table-striped">
                                    <thead>
                                        <th>#</th>
                                    	<th>Путь</th>
                                    	<th>Размер</th>
                                    </thead>
                                    <tbody>
                                        @foreach ($list as $i => $el)
                                        <tr>
                                        	<td>{{ $i + 1 }}</td>
                                        	<td><a href="{{ route('admin.dbdump.download', ['path' => $el['path']]) }}">{{ $el['path'] }}</a></td>
                                        	<td>{{ $el['size'] }}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>