
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Список тегов</h4>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table class="table table-striped">
                                    <thead>
                                        <th>#</th>
                                    	<th>Название</th>
                                    	<th>Код</th>
                                    </thead>
                                    <tbody>
                                        @foreach ($list as $el)
                                        <tr>
                                        	<td>{{ $el['id'] }}</td>
                                        	<td>{{ $el['name'] }}</td>
                                        	<td>{{ $el['code'] }}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>