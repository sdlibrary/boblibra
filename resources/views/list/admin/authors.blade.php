
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Список авторов</h4>
                                <p class="category">Можно добавить подзаголовок</p>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table class="table table-striped">
                                    <thead>
                                        <th>#</th>
                                    	<th>Имя</th>
                                    	<th>ID (ozon.ru)</th>
                                    	<th>Ссылка (ozon.ru)</th>
                                    </thead>
                                    <tbody>
                                        @foreach ($list as $el)
                                        <tr>
                                        	<td>{{ $el['id'] }}</td>
                                        	<td><a href="/admin/authors/{{ $el['id'] }}">{{ $el['name'] }}</a></td>
                                        	<td>{{ $el['ozon_id'] }}</td>
                                        	<td>@if ($el['ozon_url'])<a href="{{ $el['ozon_url'] }}">{{ $el['ozon_url'] }}</a>@endif</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>