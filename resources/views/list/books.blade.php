
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title pull-left">Список книг</h4>
                            </div>
                                <table class="table table-striped">
                                    <thead>
                                        <th>#</th>
                                    	<th>Обложка</th>
                                    	<th>Заголовок</th>
                                    	<th>Автор</th>
                                    	<th>ID (ozon.ru)</th>
                                        <th>Ссылка (ozon.ru)</th>
                                    </thead>
                                    <tbody>
                                        @foreach ($list as $el)
                                        <tr>
                                        	<td>{{ $el['id'] }}</td>
                                        	<td>
                                            @if (!empty($el['cover']))
                                                <img class="book-cover zoom" src="{{ $el['cover'] }}"/>
                                            @endif
                                            </td>
                                        	<td><a href="/books/{{ $el['id'] }}">{{ $el['name'] }}</a></td>
                                        	<td>
                                            @if (!empty($el->authors()))
                                                @foreach ($el->authors()->get() as $authorIndex => $author)
                                                <a href="/authors/{{ $author->id }}/">{{ $author->name }}</a>{{
                                                    $authorIndex < $el->authors()->count() - 1 ? ', ' : ''
                                                }}
                                                @endforeach
                                            @endif
                                            </td>
                                        	<td>{{ $el['ozon_id'] }}</td>
                                            <td>@if ($el['ozon_url'])<a target="_blank" href="{{ $el['ozon_url'] }}">{{ $el['ozon_url'] }}</a>@endif</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>

                        </div>
                    </div>
                </div>