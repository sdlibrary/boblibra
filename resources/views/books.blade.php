@extends('layouts.app')

@section('content')
    @include('search.books')
    @include('list.books')
@endsection