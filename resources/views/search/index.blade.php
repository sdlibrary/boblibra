@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Поиск</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form class="typeahead" role="search">
                        <div class="form-group">
                            <input type="search" name="q" class="form-control search-input" placeholder="Search" autocomplete="off">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @if (!empty($results))
    </div>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            @if (!empty($results['authors']))
                <hr/>
                @if (count($results['authors']))
                <p>Авторы:</p>
                <ul>
                    @foreach ($results['authors'] as $author)
                    <li>{{ $author['name'] }} [{{ $author['id'] }}]</li>
                    @endforeach
                </ul>
                @else
                    <p>Подходящих авторов не найдено.</p>
                @endif
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            @if (!empty($results['books']))
                <hr/>
                @if (count($results['books']))
                <p>Книги:</p>
                <ul>
                    @foreach ($results['books'] as $book)
                    <li>{{ $book['name'] }} [{{ $book['id'] }}]</li>
                    @endforeach
                </ul>
                @else
                    <p>Подходящих книг не найдено.</p>
                @endif
            @endif
        </div>
    </div>
    @endif
@endsection
