
        <div class="row">
            <div class="col-md-12">
            <h2>Фильтр</h2>
            {!! Form::open(['route' => 'books', 'method' => 'get']) !!}
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::text('query', '', ['class' => 'form-control border-input clearable', 'placeholder' => "Введите искомое"]) !!}
                        <span class="input-group-addon">
                            <div class="form-check">
                                <label>
                                    <input type="checkbox" name="search-authors" {!! $doSearchAuthors ? 'checked="checked"' : '' !!}><span class="label-text">Поиск по авторам</span>
                                </label>
                            </div>
                        </span>
                        <span class="input-group-btn">
                            <button class="btn btn-default clear-input" type="button"><i class="ti-close"></i></button>
                        </span>
                    </div>
                </div>
                <div class="text-center">
                    <button type="submit" class="btn btn-primary btn-fill btn-wd">Искать</button>
                </div>
                <div class="clearfix"></div>
            {!! Form::close() !!}
            </div>
        </div>